package com.bigfans.paymentservice.model;

import java.io.Serializable;


public class PayResult implements Serializable{

	private static final long serialVersionUID = -864879414487582541L;

	public enum PayStatus {
		SUCCESS, FAILED, UNKNOWN
	}

	private String tradeNo;
	private String qrImgPath;
	private String qrCode;
	private PayStatus status;

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getQrImgPath() {
		return qrImgPath;
	}

	public void setQrImgPath(String qrImgPath) {
		this.qrImgPath = qrImgPath;
	}

	public PayStatus getStatus() {
		return status;
	}

	public void setStatus(PayStatus status) {
		this.status = status;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

}
