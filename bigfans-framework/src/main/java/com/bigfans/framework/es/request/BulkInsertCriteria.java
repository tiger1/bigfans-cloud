package com.bigfans.framework.es.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.bigfans.framework.es.IndexDocument;
import lombok.Data;

/**
 * 
 * @author ellison
 *
 */
@Data
public class BulkInsertCriteria implements Serializable{

	private static final long serialVersionUID = -579084959797584167L;
	private String index;
	private String type;
	private String alias;
	private List<IndexDocument> docList = new ArrayList<IndexDocument>();

}
