package com.bigfans.framework.es.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2015年10月8日 上午8:01:18 
 * @version V1.0
 */
public class SearchResult<T> implements Serializable{

	private static final long serialVersionUID = -2103192882495410970L;
	private List<T> data;
	private Long totHitis;
	private List<AggregationResult> aggregationList = new ArrayList<AggregationResult>();

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Long getTotHitis() {
		return totHitis;
	}

	public void setTotHitis(Long totHitis) {
		this.totHitis = totHitis;
	}

	public List<AggregationResult> getAggregationList() {
		return aggregationList;
	}

	public void setAggregationList(List<AggregationResult> aggregationList) {
		this.aggregationList = aggregationList;
	}
	
}
