package com.bigfans.userservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.userservice.dao.UserPointLogDAO;
import com.bigfans.userservice.model.UserPointLog;
import org.springframework.stereotype.Repository;

@Repository
public class UserPointLogDAOImpl extends MybatisDAOImpl<UserPointLog> implements UserPointLogDAO {

}
