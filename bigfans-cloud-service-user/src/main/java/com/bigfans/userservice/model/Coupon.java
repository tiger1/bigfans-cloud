package com.bigfans.userservice.model;

import com.bigfans.userservice.model.entity.CouponEntity;
import lombok.Data;

@Data
public class Coupon extends CouponEntity {
}
