package com.bigfans.userservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:配送地址
 * @author lichong 
 * 2014年12月14日下午4:43:27
 *
 */
@Data
@Table(name="Address")
public class AddressEntity extends AbstractModel {
	
	private static final long serialVersionUID = -6286239959550734024L;
	/* 用户ID */
	@Column(name="user_id")
	protected String userId;
	@Column(name="is_primary")
	protected Boolean isPrimary;
	/* 收货人姓名 */
	@Column(name="consignee")
	protected String consignee;
	/* 电子邮箱 */
	@Column(name="email")
	protected String email;
	/* 邮编 */
	@Column(name="postalcode")
	protected String postalcode;
	/* 座机 */
	@Column(name="tel")
	protected String tel;
	/* 手机 */
	@Column(name="mobile")
	protected String mobile;
	@Column(name="province_id")
	protected String provinceId;
	@Column(name="province")
	protected String province;
	@Column(name="city_id")
	protected String cityId;
	@Column(name="city")
	protected String city;
	@Column(name="region_id")
	protected String regionId;
	@Column(name="region")
	protected String region;
	@Column(name="address")
	protected String address;
	
	@Override
	public String getModule() {
		return "Address";
	}

}
