package com.bigfans.catalogservice.service.sku;

import com.bigfans.catalogservice.model.SKU;
import com.bigfans.catalogservice.model.Stock;
import com.bigfans.framework.dao.BaseService;

import java.util.Map;

public interface StockService extends BaseService<Stock> {

    Integer orderStockOut(String orderId , String userId , Map<String , Integer> prodMap) throws Exception;

    void revertOrderUsedStock(String orderId) throws Exception;

    Stock getByProd(String pid) throws Exception;

    Stock getBySkuValKey(String valKey) throws Exception;

}
