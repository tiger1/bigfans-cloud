package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.CategoryDAO;
import com.bigfans.catalogservice.model.Category;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(CategoryDAOImpl.BEAN_NAME)
public class CategoryDAOImpl extends MybatisDAOImpl<Category> implements CategoryDAO {

	public static final String BEAN_NAME = "categoryDAO";
	
	@Override
	public List<Category> listSub(String parentId) {
		ParameterMap params = new ParameterMap();
		params.put("parentId", parentId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public String getParentId(String catId) {
		ParameterMap params = new ParameterMap();
		params.put("catId", catId);
		return getSqlSession().selectOne(className + ".getParentId", params);
	}

	@Override
	public List<Category> listByLevel(Integer level) {
		ParameterMap params = new ParameterMap();
		params.put("level", level);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	public List<Category> listByLevel(Integer level , String parentId , boolean showInNav) {
		ParameterMap params = new ParameterMap();
		params.put("level", level);
		params.put("parentId", parentId);
		params.put("showInNav", showInNav);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<Category> listLevel1(boolean showInNav) {
		return listByLevel(1 , null , showInNav);
	}

	@Override
	public List<Category> listLevel2(String parentId, boolean showInNav) {
		return listByLevel(2 , parentId , showInNav);
	}

	@Override
	public List<Category> listLevel3(String parentId, boolean showInNav) {
		return listByLevel(3 , parentId , showInNav);
	}

}
