package com.bigfans.catalogservice.model;


import com.bigfans.catalogservice.model.entity.ProductImageEntity;
import lombok.Data;

/**
 * 
 * @Description:商品图片
 * @author lichong 2015年3月18日上午10:45:13
 *
 */
@Data
public class ProductImage extends ProductImageEntity {
	
	private static final long serialVersionUID = 2597093499008798703L;
	private String ext;
	private String originName;
	
	@Override
	public ProductImage clone() throws CloneNotSupportedException {
		return (ProductImage) super.clone();
	}

}
