package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.ProductGroupEntity;
import lombok.Data;

import java.util.List;

/**
 * 
 * @Description: 商品组
 * @author lichong 
 * 2015年3月18日上午10:48:41
 *
 */
@Data
public class ProductGroup extends ProductGroupEntity {

	private static final long serialVersionUID = 968128090789953974L;
	private String categoryName;
	private String brandName;
	
	private List<SpecOption> specList;
	
	private String categoryId;
	private String[] categoryIdList;
	
}
