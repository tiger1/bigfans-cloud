package com.bigfans.searchservice.schema.mapping;

import com.bigfans.framework.es.BaseMapping;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class OrderMapping extends BaseMapping {

	public static final String INDEX = "order";
	public static final String TYPE = "Order";
	public static final String ALIAS = "order_candidate";
	public static final String FIELD_ID = "id";
	public static final String FIELD_CREATE_DATE = "ceateDate";
	public static final String FIELD_UPDATE_DATE = "updateDate";
	public static final String FIELD_USERID = "userId";
	public static final String FIELD_USERNAME = "userNickname";
	public static final String FIELD_TOTALPRICE = "totalPrice";
	public static final String FIELD_GAINED_POINTS = "gainedPoints";
	public static final String FIELD_ITEMS = "items";
	
	@Override
	public String getType() {
		return TYPE;
	}
	
	@Override
	public String getIndex() {
		return INDEX;
	}
	
	public Object getMapping() {
		XContentBuilder schemaBuilder = null;
		try {
			schemaBuilder = XContentFactory.jsonBuilder()
					.startObject()
						.startObject(TYPE)
							.startObject("properties")
								.startObject(FIELD_ID)
									.field("type", "keyword")
								.endObject()
								.startObject(FIELD_CREATE_DATE)  
						            .field("type", "date")  
						        .endObject()
						        .startObject(FIELD_UPDATE_DATE)  
						            .field("type", "date")  
						        .endObject()
						        .startObject(FIELD_ITEMS)  
						            .field("type", "nested")  
					            .endObject()
						        .startObject(FIELD_USERID)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_USERNAME)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_TOTALPRICE)  
						            .field("type", "double")  
						        .endObject()
						        .startObject(FIELD_GAINED_POINTS)  
						            .field("type", "double")  
						        .endObject()
						     .endObject()
						 .endObject()
					.endObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schemaBuilder;
	}

}
