package com.bigfans.searchservice.model.vo;


/**
 * 
 * @Title: 
 * @Description: 搜索时被选中的查询filter 
 * @author lichong 
 * @date 2016年3月26日 上午10:05:53 
 * @version V1.0
 */
public class SelectedFilter {
	
	private String label;
	private String unsetUrl;
	private String key;
	private String value;
	
	public SelectedFilter(String key , String label , String unsetUrl) {
		this.key = key;
		this.label = label;
		this.unsetUrl = unsetUrl;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUnsetUrl() {
		return unsetUrl;
	}

	public void setUnsetUrl(String unsetUrl) {
		this.unsetUrl = unsetUrl;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
