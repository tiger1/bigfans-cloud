package com.bigfans.model.event.user;

import lombok.Data;

@Data
public class UserLoginEvent {

    private String tempToken;
    private String token;

    public UserLoginEvent(String tempToken, String token) {
        this.tempToken = tempToken;
        this.token = token;
    }
}
