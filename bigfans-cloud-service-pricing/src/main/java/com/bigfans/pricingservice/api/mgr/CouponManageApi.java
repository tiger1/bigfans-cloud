package com.bigfans.pricingservice.api.mgr;

import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.pricingservice.model.Coupon;
import com.bigfans.pricingservice.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-08-04 下午6:10
 **/
@RestController("/coupon")
public class CouponManageApi extends BaseController {

    @Autowired
    private CouponService couponService;

    @PostMapping("/create")
    @NeedLogin(roles = "admin")
    public RestResponse create(@RequestBody Coupon coupon) throws Exception {
        couponService.create(coupon);
        return RestResponse.ok();
    }

}
