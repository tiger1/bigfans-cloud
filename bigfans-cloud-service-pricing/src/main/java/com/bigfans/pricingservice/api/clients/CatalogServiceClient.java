package com.bigfans.pricingservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.pricingservice.PricingApplications;
import com.bigfans.pricingservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Product> getProductById(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate , PricingApplications.getFunctionalUser());
            Map data = serviceRequest.get(Map.class , "http://catalog-service/attributes?prodId={prodId}" , prodId);
            Product product = BeanUtils.mapToModel(data , Product.class);
            return product;
        });
    }
}
